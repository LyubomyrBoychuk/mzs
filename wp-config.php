<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'consul');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^|`9/!Xh`xC0sjx!iv__6a_*-?)LAsG+Y.ij9L%kIO`rk`}Mo`r-yl/A4Mrtxd`0');
define('SECURE_AUTH_KEY',  'ZZMD.DYv8G@#MU}uuhgU4F5DxiF<=w -OgBkZkVBLw[e{AeZyhhA(M{$Tz7uPJF$');
define('LOGGED_IN_KEY',    'Pg|OjAJl.pLzf<oe&NXw<Mj`v->^K1a>v+(SEG/fJQ`:J3L-=Md!*5y)*OK15&Uv');
define('NONCE_KEY',        'C(;JzNiSFs{(`Vf~+^j- s-|79c-~MMfriB|{=Pwz!y|9]&Hp?f{8/&q&s|BkBHW');
define('AUTH_SALT',        'r_|^&RT.$[?+rK:c+Aemn|r-rVWE}V|6a>Fc3hc%eSa0N7W-k.Z793vwk{$U>$Tq');
define('SECURE_AUTH_SALT', 'Np36 n*)=a@+enD;eNg0.4i^!x[7xm|RPtV;hZ%vkZ&-}7l-aeo[:d|m<T2j,(p-');
define('LOGGED_IN_SALT',   'yoyZ7>+39o8@TpO(w*<649%=DR{ITM?@ZZw(Fd&G-r c3bi77]8~iFh;Dr}g7:J-');
define('NONCE_SALT',       '}O4fYnqAH:/M!=,t[0xFr|W1zKLxd rL]l,9P7b^Izoe.&ZvqD>,Cg7)GC|o%Ik ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
